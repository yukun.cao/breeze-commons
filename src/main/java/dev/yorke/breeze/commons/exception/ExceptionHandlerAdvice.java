package dev.yorke.breeze.commons.exception;

import dev.yorke.breeze.commons.result.BreezeResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author Yorke
 */
@Slf4j
@RestControllerAdvice
public class ExceptionHandlerAdvice {

    @ExceptionHandler(BreezeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BreezeResult handleBreezeException(BreezeException e) {
        log.error(e.getMessage(), e);
        return BreezeResult.failure(e);
    }

    @ExceptionHandler(BreezeAuthException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public BreezeResult handleBreezeAuthException(BreezeAuthException e) {
        log.error(e.getMessage(), e);
        return BreezeResult.failure(e);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BreezeResult handleException(Exception e) {
        log.error(e.getMessage(), e);
        return BreezeResult.failure(e.getMessage());
    }
}
