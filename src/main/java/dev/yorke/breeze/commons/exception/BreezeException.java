package dev.yorke.breeze.commons.exception;

import dev.yorke.breeze.commons.ResultStatus;

public class BreezeException extends BaseException {
    public BreezeException(ResultStatus resultStatus) {
        super(resultStatus);
    }

    public BreezeException(String message) {
        super(message);
    }

    public BreezeException(ResultStatus resultStatus, String message) {
        super(resultStatus, message);
    }
}
