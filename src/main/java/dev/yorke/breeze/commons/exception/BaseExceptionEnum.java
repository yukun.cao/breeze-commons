package dev.yorke.breeze.commons.exception;

import dev.yorke.breeze.commons.ResultStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Yorke
 */
@Getter
@AllArgsConstructor
public enum BaseExceptionEnum implements ResultStatus {
    /**
     * 成功
     */
    SUCCESS(20000, "成功！"),
    /**
     * 默认异常
     */
    EXECPTION(40000, "系统异常！"),
    /**
     * 默认认证异常
     */
    AUTH_EXECPTION(60000, "认证失败！");

    private int code;

    private String message;
}
