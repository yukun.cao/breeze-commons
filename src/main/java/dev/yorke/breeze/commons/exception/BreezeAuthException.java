package dev.yorke.breeze.commons.exception;

/**
 * @author caoyk
 */
public class BreezeAuthException extends BaseException {

    public BreezeAuthException(String message) {
        super(message);
    }
}
