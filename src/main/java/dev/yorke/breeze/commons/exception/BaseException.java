package dev.yorke.breeze.commons.exception;

import dev.yorke.breeze.commons.ResultStatus;
import lombok.Getter;

/**
 * @author Yorke
 */
public class BaseException extends RuntimeException implements ResultStatus {

    @Getter
    protected int code;

    public BaseException(ResultStatus resultStatus) {
        super(resultStatus.getMessage());
        this.code = resultStatus.getCode();
    }

    public BaseException(String message) {
        super(message);
        this.code = BaseExceptionEnum.EXECPTION.getCode();
    }

    public BaseException(ResultStatus resultStatus, String message) {
        super(message);
        this.code = resultStatus.getCode();
    }
}
