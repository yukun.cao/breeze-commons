package dev.yorke.breeze.commons;

/**
 * @author Yorke
 */
public interface ResultStatus {

    /**
     * 获取状态码
     *
     * @return 状态码
     */
    int getCode();

    /**
     * 获取状态消息
     *
     * @return 状态消息
     */
    String getMessage();
}
