package dev.yorke.breeze.commons.result;

import dev.yorke.breeze.commons.ResultStatus;
import dev.yorke.breeze.commons.exception.BaseExceptionEnum;
import lombok.Data;

/**
 * @author Yorke
 */
@Data
public class BreezeResult implements ResultStatus {

    private int code;

    private String message;

    private Object data;

    private BreezeResult(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static BreezeResult success() {
        return new BreezeResult(BaseExceptionEnum.SUCCESS.getCode(), BaseExceptionEnum.SUCCESS.getMessage(), null);
    }

    public static BreezeResult success(Object data) {
        return new BreezeResult(BaseExceptionEnum.SUCCESS.getCode(), BaseExceptionEnum.SUCCESS.getMessage(), data);
    }

    public static BreezeResult failure(ResultStatus resultStatus) {
        return new BreezeResult(resultStatus.getCode(), resultStatus.getMessage(), null);
    }

    public static BreezeResult failure(String message) {
        return new BreezeResult(BaseExceptionEnum.EXECPTION.getCode(), message, null);
    }

    public static BreezeResult failure(ResultStatus resultStatus, String message) {
        return new BreezeResult(resultStatus.getCode(), message, null);
    }
}
